# iterate through list and return node with lowest elevation
def getBestKey(dict):
	# iterate through list and return node with lowest elevation
	min = 10000
	minKey = 0
	for key in dict:
		if( dict[key][0] < min ):
			min = dict[key][0]
			minKey = key
	return minKey

def returnPossiblePaths( position, cave ):
	x, pos, velocity = position
	# calculate new position
	if( velocity > -3 ):
		velocity0 = velocity - 1
	else:
		velocity0 = velocity

	if( velocity < 3 ):
		velocity1 = velocity + 1
	else:
		velocity1 = velocity

	y0 = pos + velocity0
	y1 = pos + velocity1
	
	x0 = x1 = x + 5
	
	# return tuple with (position throttle = 0, position throttle = 1)
	return [(x0, y0, velocity0), (x1, y1, velocity1)]

def checkNodeCollision( node, cave ):
	return cave.checkCollision(node[0], node[1])
	# return true if collision detected in cave

def findBestRoute(helicopter, cave, goal):
	# List [ (x, y, speed) ] = [ "elevation", move, parent (x, y, speed) ]
	openList = {}
	closedList = {}
	
	movesList = []
	pathList = []
	
	goalFound = False
	
	# Add initial node to open list (Parent node = 0, move = 0)
	openList[ (helicopter.getx(), helicopter.getPos(), helicopter.getVelocity()) ] = [cave.getElevation(helicopter.getx(), helicopter.getPos()), 0, 0 ]
	
	# While the open list is not empty and the goal isn't in the closed list
	while openList and not goalFound:
		# Start looking at the best node in the open list and move it to the closed list
		currentPosition = getBestKey(openList)
		closedList[ currentPosition ] = currentNode =  openList.pop( currentPosition )

		branchList = returnPossiblePaths(currentPosition, cave)

		for throttle,possibleBranch in enumerate(branchList):
			if not checkNodeCollision(possibleBranch, cave):
				if possibleBranch[0] >= goal:
					goalFound = True
				# Add to the openList if it's not already in the openList or the closedList
				if possibleBranch not in openList and possibleBranch not in closedList:
					# openList[(x, y, v of possible branch)] = [elevation, throttle, parent]
					openList[possibleBranch] = [cave.getElevation(possibleBranch[0], possibleBranch[1]), throttle, currentPosition]
	print "Open list: ", len(openList), "Closed list: ", len(closedList)
	# Loop backwards and record the "moves" (throttle)
	while( closedList[currentPosition][2] != 0 ): # Start with the last node (currentPosition) and continue until we hit the root
		pathList.insert(0, list(currentPosition))
		movesList.insert(0, closedList[currentPosition][1])
		currentPosition = closedList[currentPosition][2] # set the parent node as the next node

	#return movesList
	return [movesList, pathList]
