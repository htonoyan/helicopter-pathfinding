Cave class:
	keep track of blocks (list of rectangles, tuples, or objects?)
	Create new blocks based on speed
		Determine whether going up or down from previous height
	Shift blocks based on speed
	Delete old blocks?
	Collision detection
		iterate through list of blocks and do collision detection
	Public Methods:	Shift blocks
					Detect collision
					Draw cave

Helicopter class:
	Keep track of position and velocity
	Public Methods:	Update position ( throttle )
					Draw helicopter
					
route Detection:
	detect route based on current helicopter position, cave speed, cave collision detection
		plan route to edge of screen
		detect if new obstacles are in the way of the planned route
			
	draw route