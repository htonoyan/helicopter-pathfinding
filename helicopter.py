import pygame
import random
import AI
from time import sleep
pygame.init()

#Global variables
size = ScreenWidth, ScreenHeight = 500, 500
minCaveHeight = 200

screen = pygame.display.set_mode(size)

class Cave:
	# Randomly generated side-scrolling background including obstacles

	#initial values / constants
	caveColor = (0, 155, 0)
	obstacleWidth = 25
	obstaclePadding = 100
	minObstacleHeight = 50
	
	initialCaveTop = 100
	blockWidth = 5
	initialHeight = 400
	
	obstaclePositionList = [100, 0, 100, 200]
	obstacleHeightList = [300, 200, 200, 300]
	
	def getCaveHeight(self):
		return self.caveHeight
		
	def decrementCaveHeight(self):
		self.caveHeight -= 1
		
	def __init__(self):
		self.topBlocks = []
		self.bottomBlocks = []
		self.obstacles = []
		self.caveTop = self.initialCaveTop
		self.caveHeight = self.initialHeight
		
		for i in range(0, ScreenWidth, self.blockWidth):
			self.addBlock()

	def addBlock(self):
		rand = random.randint(0,2)
		if( rand == 0 ):
			if( self.caveTop + self.caveHeight < ScreenHeight ):
				self.caveTop += 3
		elif(rand == 1):
			if( self.caveTop > 0 ):
				self.caveTop -= 3
				
		# Custom map
		self.caveTop = 10
		# / Custom map
		
		self.topBlocks.extend(self.caveTop for x in range(self.blockWidth)) # Add block coordinates for top blocks
		self.bottomBlocks.extend(self.caveTop + self.caveHeight for x in range(self.blockWidth)) # add block coordinates for bottom blocks

	def addObstacle(self, x):
		limitBottom = self.bottomBlocks[-1]
		limitTop = self.topBlocks[-1]
		randomHeight = random.randint(self.minObstacleHeight, limitBottom - limitTop - self.obstaclePadding)
		randomPosition = random.randint(limitTop, limitBottom - randomHeight)
		
		# Add obstacle to obstacle list

		# Custom map
		# self.obstacles.append(pygame.Rect(x, randomPosition, self.obstacleWidth, randomHeight))
		self.obstacles.append(pygame.Rect(x, self.obstaclePositionList[0], self.obstacleWidth, self.obstacleHeightList[0]))
		self.obstaclePositionList.append(self.obstaclePositionList.pop(0))
		self.obstacleHeightList.append(self.obstacleHeightList.pop(0))
		# / Custom Map
	def drawCave(self):
		for x in range(0,ScreenWidth-1, 1):
			pygame.draw.rect(screen, self.caveColor, pygame.Rect(x, 0, 1, self.topBlocks[x]))
			pygame.draw.rect(screen, self.caveColor, pygame.Rect(x, self.bottomBlocks[x], 1, ScreenHeight - self.bottomBlocks[x]))

		for blocks in self.obstacles:
			pygame.draw.rect(screen, self.caveColor, blocks, 0)

	def update(self):
		del self.topBlocks[0:self.blockWidth]
		del self.bottomBlocks[0:self.blockWidth]
		self.addBlock()

		for block in self.obstacles:
			block.move_ip(-self.blockWidth,0)
			if block.left <= 0:
				del block
	def checkCollision(self, x, y):
		if y < self.topBlocks[x]:
			return 1
		if y > self.bottomBlocks[x]:
			return 1
		for block in self.obstacles:
			if block.collidepoint(x, y):
				return 1
		return 0
		
	def getElevation(self, x, y):
		obstacleMax = 0
		peak = 500 # peak at center of obstacle
		length = 500 # length to base of obstacle
		buffer = 100 # buffer height around obstacle
		
		tilt = 500 # Tilt is the added "height" to the left of the screen, and is 0 at the right.
		
		elevateTop = 300 - (y - self.topBlocks[x]) + tilt - x*tilt/ScreenWidth
		elevateBottom = 300 - (self.bottomBlocks[x] - y) + tilt - x*tilt/ScreenWidth

		for block in self.obstacles:
			if x < block.right:
				# If we're to the left of an obstacle, the "height" starts at peak near the center and decreases as you get further away
				if x < block.left:
					# Left half
					obstacleScore = peak - (peak/length)*(block.left - x) - (peak/(block.height/2 + buffer))*abs(y - block.center[1])+ tilt - x*tilt/ScreenWidth
				else:
					# Either directly above or under the block
					obstacleScore = peak - (peak/(block.height/2 + buffer))*abs(y - block.center[1]) + tilt - x*tilt/ScreenWidth
				
				if obstacleScore > obstacleMax:
					obstacleMax = obstacleScore

		return max(elevateTop, elevateBottom, obstacleMax)

class helicopter:
	# Moves up and down with acceleration, draws self to screen
	def __init__(self):
		self.x = 100
		self.position = 250
		self.velocity = 0
		self.draw()
	
	def update(self, throttle):
		if( not throttle ):
			if( self.velocity > -3 ):
				self.velocity -= 1
		else:
			if( self.velocity < 3 ):
				self.velocity += 1
		
		self.position += self.velocity
	
	def getPos(self):
		return self.position
		
	def getVelocity(self):
		return self.velocity
	
	def draw(self):
		pygame.draw.circle(screen, (200,0,0), (self.x, self.position), 5, 0)
		
	def getx(self):
		return self.x

class score:
# Keeps score and prints to screen
	color = (255,255,255)
	
	def __init__(self):
		self.currentScore = 0
		self.scoreFont = pygame.font.Font(None, 15)
		
	def draw(self):
		scoreSurface = self.scoreFont.render(str(self.currentScore),False,self.color,(0,0,0))
		pygame.display.get_surface().blit(scoreSurface,(ScreenWidth - scoreSurface.get_width(),ScreenHeight - scoreSurface.get_height()),None, 0)
		
	def update(self):
		self.currentScore += 1

#initialize
helicopter = helicopter()
gameCave = Cave()
score = score()

heightCounter = 0
obstacleCounter = 0
pathCounter = 0
clock = pygame.time.Clock()

pygame.font.get_init()

movesList, pathList = AI.findBestRoute(helicopter, gameCave, 450)

while (gameCave.checkCollision(helicopter.getx(), helicopter.getPos()) == 0):
	clock.tick(60)
	pygame.event.get()

	#update
	gameCave.update()
	score.update()
#	helicopter.update(pygame.mouse.get_pressed()[0])
	helicopter.update(movesList.pop(0))


	#drawing
	screen.fill((0,0,0))
	gameCave.drawCave()
	helicopter.draw()
	score.draw()
	for node in pathList:
		pygame.draw.circle(screen, (200,200,200), (node[0], node[1]), 5, 0)
		node[0] -= 5
	del pathList[0]
	pygame.display.flip()

	#adjust difficulty timers
	if gameCave.getCaveHeight() > minCaveHeight :
		heightCounter += 1
		if heightCounter > 20 :
			heightCounter = 0
			gameCave.decrementCaveHeight()
		
	obstacleCounter += 1
	if obstacleCounter > 40 :
		obstacleCounter = 0
		gameCave.addObstacle(ScreenWidth)
	
	pathCounter += 1
	if pathCounter > 10:
		pathCounter = 0
		# draw best route on screen
		
#		for x in range(ScreenWidth):
#			for y in range(ScreenHeight):
#				if not gameCave.checkCollision(x, y):
#					height = gameCave.getElevation(x, y)*255/1500
#					pygame.draw.rect(screen, (height, height, height), pygame.Rect(x, y, 2, 2), 0)
		
		movesList, pathList = AI.findBestRoute(helicopter, gameCave, 450)
#		for node in pathList:
#			pygame.draw.circle(screen, (200,200,200), (node[0], node[1]), 5, 0)

#		pygame.display.flip()
#		sleep(1)

	#quit
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			pygame.quit()
			quit()